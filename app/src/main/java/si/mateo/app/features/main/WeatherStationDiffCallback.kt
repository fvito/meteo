package si.mateo.app.features.main

import android.support.v7.util.DiffUtil
import si.mateo.app.data.models.WeatherStation

class WeatherStationDiffCallback: DiffUtil.ItemCallback<WeatherStation>() {

    override fun areItemsTheSame(oldItem: WeatherStation, newItem: WeatherStation): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(newItem: WeatherStation, oldItem: WeatherStation): Boolean {
        return oldItem == newItem
    }

}
