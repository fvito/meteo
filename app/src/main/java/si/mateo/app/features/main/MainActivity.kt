package si.mateo.app.features.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import si.mateo.app.R
import si.mateo.app.data.WeatherRepository
import si.mateo.app.data.local.AppDatabase
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.data.remote.Resource
import si.mateo.app.data.remote.Status
import si.mateo.app.data.remote.Webservice
import si.mateo.app.viewmodels.WeatherListViewModel
import si.mateo.app.viewmodels.WeatherListViewModelFactory

class MainActivity : AppCompatActivity() {

    private val uiScope = CoroutineScope(Dispatchers.Main)

    private lateinit var viewModel: WeatherListViewModel
    private lateinit var factory: WeatherListViewModelFactory
    private lateinit var repository: WeatherRepository

    private var refreshJob = Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        repository = WeatherRepository(AppDatabase.getInstance(this@MainActivity).weatherDao(), Webservice())
        factory = WeatherListViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(WeatherListViewModel::class.java)

        val listAdapter = WeatherStationsAdapter()

        weather_stations_list.apply {
            adapter = listAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        refresh_container.setOnRefreshListener {
            refreshJob.cancel()
            refreshJob = Job()
            fetchData()
        }
        updateUi(listAdapter)
    }

    private fun updateUi(adapter: WeatherStationsAdapter) {
        viewModel.getWeatherStations().observe(this, Observer<Resource<List<WeatherStation>>> { resource ->
            resource?.let {
                when (resource.status){
                    Status.ERROR -> {
                        refresh_container.isRefreshing = false
                        resource.message?.let { it1 -> Snackbar.make(root_layout, it1, Snackbar.LENGTH_LONG).show() }
                    }
                    Status.LOADING -> {
                        refresh_container.isRefreshing = true
                    }
                    Status.SUCCESS -> {
                        Log.d("METEO", "Fetch successful!")
                        refresh_container.isRefreshing = false
                        adapter.submitList(resource.data)
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        fetchData()
    }

    override fun onPause() {
        super.onPause()
        refreshJob.cancel()
    }

    private fun fetchData() {
        refresh_container.isRefreshing = false
        Log.d("METEO", "FetchData")
        uiScope.launch(Dispatchers.Main + refreshJob) {
            while (true) {
                withContext(Dispatchers.Main) {viewModel.refresh() }
                delay(30000)
            }
        }
    }
}
