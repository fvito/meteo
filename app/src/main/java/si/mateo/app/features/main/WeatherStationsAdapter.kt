package si.mateo.app.features.main

import android.graphics.Color
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import si.mateo.app.R
import si.mateo.app.data.models.WeatherStation
import kotlinx.android.synthetic.main.list_item_weather_station.view.*
import si.mateo.app.utils.Utils
import si.mateo.app.utils.loadImageFromUrl

class WeatherStationsAdapter: ListAdapter<WeatherStation, WeatherStationsAdapter.ViewHolder>(WeatherStationDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_weather_station, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val station = getItem(position)
        viewHolder.bind(station)
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(station: WeatherStation) {
            itemView.station_name.text = station.title
            itemView.weather_temperature.text = String.format("%d °C", station.temperature)
            itemView.wind_info.text = itemView.resources.getString(R.string.station_wind_info, station.windDirection, station.windSpeed)
            itemView.weather_icon.setImageResource(0)
            station.weatherIcon?.let { itemView.weather_icon.loadImageFromUrl(Utils.getIconUrl(it) ) }
            itemView.wind_icon.setImageResource(0)
            station.windIcon?.let { itemView.wind_icon.loadImageFromUrl(Utils.getIconUrl(it)) }
            itemView.station_issued.text = station.dateIssued
            if(station.temperature > 0) {
                itemView.station_background.setBackgroundColor(Color.GREEN)
            }else {
                itemView.station_background.setBackgroundColor(Color.WHITE)
            }
        }
    }

}
