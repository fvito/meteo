package si.mateo.app.utils

object Utils{
    const val API_URL = "http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_si_latest.xml"
    const val DB_NAME = "meteo"

    fun getIconUrl(icon: String): String = "http://www.meteo.si/uploads/meteo/style/img/weather/$icon.png"

}