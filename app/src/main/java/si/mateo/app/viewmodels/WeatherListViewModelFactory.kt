package si.mateo.app.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import si.mateo.app.data.WeatherRepository
import si.meteo.app.testing.OpenForTesting

@OpenForTesting
class WeatherListViewModelFactory(private val repository: WeatherRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeatherListViewModel(repository) as T
    }
}