package si.mateo.app.viewmodels

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.ViewModel
import si.mateo.app.data.WeatherRepository
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.data.remote.Resource
import si.meteo.app.testing.OpenForTesting

@OpenForTesting
class WeatherListViewModel internal constructor(private val repository: WeatherRepository): ViewModel() {

    private var weatherList = MediatorLiveData<Resource<List<WeatherStation>>>()

    /*init {
        weatherList.addSource(repository.getStations(true), weatherList::setValue)
    }
    */

    fun getWeatherStations() = weatherList

    fun refresh() {
        weatherList.removeSource(repository.getStations())
        weatherList.addSource(repository.getStations(true), weatherList::setValue)
    }

}