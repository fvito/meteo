package si.mateo.app.data

import android.arch.lifecycle.LiveData
import si.mateo.app.AppExecutors
import si.mateo.app.data.local.WeatherDao
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.data.remote.NetworkBoundResource
import si.mateo.app.data.remote.Resource
import si.mateo.app.data.remote.Webservice

class WeatherRepository(private val weatherDao: WeatherDao, private val webservice: Webservice) {
    private val appExecutors = AppExecutors()

    fun getStations(fetch: Boolean = true): LiveData<Resource<List<WeatherStation>>> {
        return object : NetworkBoundResource<List<WeatherStation>, List<WeatherStation>>(appExecutors) {
            override fun saveCallResult(item: List<WeatherStation>) {
                weatherDao.saveWeatherStations(item)
            }

            override fun shouldFetch(data: List<WeatherStation>?): Boolean {
                return fetch
            }

            override fun loadFromDb(): LiveData<List<WeatherStation>> {
                return weatherDao.getAllWeatherStations()
            }

            override fun createCall(): LiveData<Resource<List<WeatherStation>>> {
                return webservice.getWeatherStations()
            }

        }.asLiveData()
    }
}