package si.mateo.app.data

import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import si.mateo.app.data.models.WeatherStation
import java.io.IOException
import java.io.InputStream
import java.lang.IllegalStateException

class WeatherStationsXmlParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): List<WeatherStation> {
        inputStream.use {
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(it, null)
            parser.nextTag()
            return readWeatherStations(parser)
        }
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readWeatherStations(parser: XmlPullParser): List<WeatherStation> {
        val stations = mutableListOf<WeatherStation>()
        parser.require(XmlPullParser.START_TAG, null, "data")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == "metData") {
                stations.add(readStation(parser))
            } else {
                skip(parser)
            }
        }
        return stations
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readStation(parser: XmlPullParser): WeatherStation {
        parser.require(XmlPullParser.START_TAG, null, "metData")
        var id: String = "" //domain_meteosiId
        var title: String = "" //domain_shortTitle
        var dateIssued: String = "" //tsValid_issued
        var temperature: Int = 0 //t_degrees
        var weatherIcon: String? = null //nn_icon
        var windSpeed: Float? = null //ff_val
        var windDirection: String = "" //dd_shortText
        var windIcon: String? = null //ff_icon

        while (parser.next() != XmlPullParser.END_TAG) {
            if(parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when(parser.name) {
                "domain_meteosiId" -> id = readTag(parser, null, "domain_meteosiId")
                "domain_longTitle" -> title = readTag(parser, null, "domain_longTitle")
                "tsValid_issued" -> dateIssued = readTag(parser, null, "tsValid_issued")
                "t_degreesC" -> temperature = readTag(parser, null, "t_degreesC").toIntOrNull() ?: 0
                "nn_icon" -> weatherIcon = readNullableTag(parser, null, "nn_icon")
                "ff_val" -> windSpeed = readTag(parser, null, "ff_val").toFloatOrNull()
                "dd_shortText" -> windDirection = readTag(parser, null, "dd_shortText")
                "ddff_icon" -> windIcon = readNullableTag(parser, null, "ddff_icon")
                else -> skip(parser)
            }
        }
        return WeatherStation(id, title, dateIssued, temperature, weatherIcon, windSpeed, windDirection, windIcon)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if(parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }

    private fun readTag(parser: XmlPullParser, namespace: String?, tag: String) : String {
        parser.require(XmlPullParser.START_TAG, namespace, tag)
        val result = readText(parser)
        parser.require(XmlPullParser.END_TAG, namespace, tag)
        return result
    }

    private fun readNullableTag(parser: XmlPullParser, namespace: String?, tag: String) : String? {
        parser.require(XmlPullParser.START_TAG, namespace, tag)
        val result = readText(parser)
        parser.require(XmlPullParser.END_TAG, namespace, tag)
        if(result.isBlank()){
            return null
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if(parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0){
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }
}