package si.mateo.app.data.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "weather_station")
data class WeatherStation(
    @PrimaryKey
    var id: String,
    @ColumnInfo(name = "title")
    var title: String,
    @ColumnInfo(name = "date")
    var dateIssued: String,
    @ColumnInfo(name = "weather_temperature")
    var temperature: Int,
    @ColumnInfo(name = "weather_icon")
    var weatherIcon: String?,
    @ColumnInfo(name = "wind_speed")
    var windSpeed: Float?,
    @ColumnInfo(name = "wind_direction")
    var windDirection: String,
    @ColumnInfo(name = "wind_icon")
    var windIcon: String?
)