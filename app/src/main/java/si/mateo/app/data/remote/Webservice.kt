package si.mateo.app.data.remote

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.*
import si.mateo.app.data.WeatherStationsXmlParser
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.utils.Utils
import java.io.IOException
import java.io.InputStream

class Webservice {
    private val client = OkHttpClient()

    fun getWeatherStations(): LiveData<Resource<List<WeatherStation>>> {
        val data = MutableLiveData<Resource<List<WeatherStation>>>()
        GlobalScope.launch {
            val request = Request.Builder().url(Utils.API_URL).build()
            try {
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    val stations = WeatherStationsXmlParser().parse(response.body()?.byteStream() as InputStream)
                    Log.d("METEO", "Konec")
                    data.postValue(Resource.success(stations))
                }
            }
            catch (ex: IOException) {
                Log.d("METEO", ex.message)
                data.postValue(Resource.error("Network error"))
            }
        }
        return data
    }

}