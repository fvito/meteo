package si.mateo.app.data.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import si.mateo.app.data.models.WeatherStation


@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveWeatherStations(stations: List<WeatherStation>)

    @Query("SELECT * FROM weather_station")
    fun getAllWeatherStations() : LiveData<List<WeatherStation>>
}