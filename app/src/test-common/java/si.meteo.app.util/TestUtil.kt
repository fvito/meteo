package si.meteo.app.util

import si.mateo.app.data.models.WeatherStation

object TestUtil {
    fun createWeatherStation(name: String) = WeatherStation(
        id = name,
        title = name,
        dateIssued = "1.3.2017",
        temperature = 0,
        weatherIcon = null,
        windSpeed = 0f,
        windDirection = "J",
        windIcon = null
    )
}