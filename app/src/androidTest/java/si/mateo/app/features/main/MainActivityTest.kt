package si.mateo.app.features.main

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import android.support.test.rule.ActivityTestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import si.mateo.app.R
import si.mateo.app.SwipeRefreshLayoutMatcher.isRefreshing
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.data.remote.Resource
import si.mateo.app.viewmodels.WeatherListViewModel

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(MainActivity::class.java, true, true)

    private lateinit var viewModel: WeatherListViewModel
    private val weatherStationsData = MutableLiveData<Resource<List<WeatherStation>>>()
    private val weatherDataMediator = MediatorLiveData<Resource<List<WeatherStation>>>()

    @Before
    fun init() {
        weatherDataMediator.addSource(weatherStationsData, weatherStationsData::setValue)
        viewModel = mock(WeatherListViewModel::class.java)
        `when`(viewModel.getWeatherStations()).thenReturn(weatherDataMediator)
    }

    @Test
    fun loading() {
        weatherStationsData.postValue(Resource.loading())
        onView(withId(R.id.refresh_container)).check(matches(isRefreshing()))
    }

    @Test
    fun showErrorSnackBar() {
        weatherStationsData.postValue(Resource.error("Snackbar error"))
        onView(withText("Snackbar error")).check(matches(isDisplayed()))
    }
}
