package si.mateo.app.data.local

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import si.meteo.app.util.LiveDataTestUtil.getValue
import si.meteo.app.util.TestUtil

@RunWith(AndroidJUnit4::class)
class WeatherDaoTest {

    private lateinit var db : AppDatabase

    @Before
    fun initDb() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AppDatabase::class.java).build()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertAndLoad() {
        val stations = listOf(TestUtil.createWeatherStation("MARIBOR - LETALIŠČE"))
        db.weatherDao().saveWeatherStations(stations)

        val loaded = getValue(db.weatherDao().getAllWeatherStations())
        assertThat(loaded, `is` (stations))
    }

}