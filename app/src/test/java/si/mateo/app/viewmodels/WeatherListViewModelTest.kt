package si.mateo.app.viewmodels

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import si.mateo.app.data.WeatherRepository
import si.mateo.app.data.models.WeatherStation
import si.mateo.app.data.remote.Resource
import si.meteo.app.util.TestUtil
import android.arch.core.executor.testing.InstantTaskExecutorRule

@RunWith(JUnit4::class)
class WeatherListViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val weatherRepositoryMock = mock(WeatherRepository::class.java)
    private val weatherViewModel = WeatherListViewModel(weatherRepositoryMock)



    private val weatherStationsData = MutableLiveData<Resource<List<WeatherStation>>>()
    private val weatherDataMediator = MediatorLiveData<Resource<List<WeatherStation>>>()

    @Before
    fun init() {
        weatherDataMediator.addSource(weatherStationsData, weatherStationsData::setValue)
        Mockito.`when`(weatherRepositoryMock.getStations()).thenReturn(weatherDataMediator)
    }

    @Test
    fun testNull() {
        assertThat(weatherViewModel, notNullValue())
        verify(weatherRepositoryMock, never()).getStations()
    }

    @Test
    fun testCallRepository() {
        weatherViewModel.getWeatherStations().observeForever(mock())
        weatherViewModel.refresh()
        verify(weatherRepositoryMock, times(2)).getStations()
    }

    @Test
    fun sendResultToUI() {
        val observer = mock<Observer<Resource<List<WeatherStation>>>>()
        val stationsData = MutableLiveData<Resource<List<WeatherStation>>>()
        val mediator = MediatorLiveData<Resource<List<WeatherStation>>>()
        mediator.addSource(stationsData, stationsData::setValue)
        `when`(weatherRepositoryMock.getStations()).thenReturn(stationsData)
        weatherViewModel.getWeatherStations().observeForever(observer)
        weatherViewModel.refresh()
        verify(observer, never()).onChanged(any())

        val testStation = TestUtil.createWeatherStation("LETALIŠČE-MARIBOR")
        val testValue = Resource.success(listOf(testStation))

        stationsData.value = testValue
        verify(observer).onChanged(testValue)

    }

}